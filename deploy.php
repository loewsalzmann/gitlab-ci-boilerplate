<?php
/* (c) Jan-Michael Loew <jloew@loewsalzmann.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Deployer;

require 'recipe/typo3.php';
require 'recipe/rsync.php';

inventory('.deploy/hosts.yml');

set('rsync_src', '.');

set('rsync', [
    'exclude' => ['.ddev', '.git', '.deploy'],
    'exclude-file' => ['./.gitignore','./.gitlab-ci.yml', './README.md', './deploy.php'], //Use absolute path to avoid possible rsync problems
    'include' => [],
    'include-file' => false,
    'filter' => [],
    'filter-file' => false,
    'filter-perdir' => false,
    'flags' => 'av', // Recursive, with compress, check based on checksum rather than time/size, preserve Executable flag
    'options' => ['delete'], //Delete after successful transfer, delete even if deleted dir is not empty
    'timeout' => 3600, //for those huge repos or crappy connection
]);

set('shared_dirs', [
]);

set('`writable_dirs`', [
]);


task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup'
]);
after('deploy:failed', 'deploy:unlock');

task('previous:release', function () {
    run('echo {{previous_release}}');
});