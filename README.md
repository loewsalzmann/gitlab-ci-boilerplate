# Barebones gitlab-CI + Docker + Deployer Setup

## Get Started

### Prerequisites
Target-Environment:
* SSH-user with read/write access to the web-root
* rsync on the target machine
* A gitlab-hosted git-repository with access to gitlab-ci and docker-based runners

### Install Dependencies
This project only depends on `deployer/deployer` as its PHP based deployment setup. See https://deployer.org/ for more information.

```
composer install
```

### Configure hosts

Navigate to `.deploy/hosts.yml` and add your server credentials for the following fields:

|||
| ------------- |-------------|
| hostname | Reachable via SSH |
| user | allowed to access web-root |
| deploy_path | root directory, will later contain releases and the shared directory |

## Deployment
Deployment is based on gitlab-ci and docker runners. If you do not have shared docker-runners available, you must provide your own.

### Configure gitlab-ci.yaml

#### Example Config
A example gitlab-ci configuration is provided.

#### Create and add SSH-Private-Key to gitlab pipeline variables
You must create a new keypair (at the moment, passphrases are not supported) and add the generated private key as a variable to gitlabs deployment variables, named `SSH_PRIVATE_KEY`.
The corresponding public-key must be placed in the authorised_keys of your target environment.

### Jobs
There are two default deployment jobs: `deploy-staging` and `deploy-production`. These are set to be based of the `php:7.4-alpine` docker-image. If you need a different version, please make sure to adapt the job config.

### Enable Runners
The config expects pipelines to be run, when the `docker` tag is present in the job. This is the default for gitlab.com shared runners, however this might change depending on your environment. Make sure, that you enable runners for your project.

## Target Environment
DeployerPHP depends on a file-structure, that provides a folder for releases, a symlink to the current release and a directory for files, that are shared between releases. A typical directory would look like this:
```
./.
./..
./current -> ./releases/n
./releases
./shared
```

Your webservers webroot should point to the webroot directory beneath the current directory, to ensure, that it is always referencing the latest release.

### Shared directories
To keep directory contents between releases (e.g. TYPO3 `fileadmin` or Neos `Data/*` ), you can add the directories to the `shared_dirs` property in `deploy.php`. All directories listed there will be symlinked from `./shared/*`.

To ensure write permissions are set correctly, add writable dirs to `writable_dirs` in `deploy.php`.